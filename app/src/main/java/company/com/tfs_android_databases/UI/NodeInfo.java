package company.com.tfs_android_databases.UI;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import java.util.ArrayList;
import java.util.List;

import company.com.tfs_android_databases.Database.AppDatabase;
import company.com.tfs_android_databases.Entity.Node;
import company.com.tfs_android_databases.Entity.NodeChild;
import company.com.tfs_android_databases.R;

public class NodeInfo extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Node node;
    private static List<Node> nodesParents;
    private static List<Node> nodesChildren;
    private static boolean currentTab = false;
    private ViewPagerAdapter adapter;

    private static final String EXTRA_INFO = "extra_info";
    private static AppDatabase database;

    public static void start(Activity activity, Node node) {
        Intent intent = new Intent(activity, NodeInfo.class);
        intent.putExtra(EXTRA_INFO, node);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.node_info);

        node = getIntent().getParcelableExtra(EXTRA_INFO);
        database = AppDatabase.getAppDatabase(getApplicationContext());

        toolbar = findViewById(R.id.htab_toolbar);
        toolbar.setTitle("ID: " + node.getId() + ", Value: " + node.getValue());
        setSupportActionBar(toolbar);

        viewPager = findViewById(R.id.htab_viewpager);


        tabLayout = findViewById(R.id.htab_tabs);
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                // while selecting the tab
                currentTab = !currentTab;
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {}
        });


        fetchData();
        setupViewPager(viewPager);
        setListener();
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected void onRestart() {
        super.onRestart();

        fetchData();
        setupViewPager(viewPager);
        setListener();
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setListener() {
        FloatingActionButton fab = findViewById(R.id.fab_1);

        fab.setOnClickListener(view -> {
            NodesList.startForResult(this, 1, node.id);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                long id = data.getLongExtra("ID", 1);
                Node node1 = database.getNodeDao().getNode(id);


                if (database.getNodeChildDao().getConnection(node.getId(), node1.getId()) != 0)
                    return;

                NodeChild nodeChild;
                if (currentTab)
                {
                    nodeChild = new NodeChild(node.getId(), node1.getId());
                    adapter.getItem(0).addNode(node1);
                } else {
                    nodeChild = new NodeChild(node1.getId(), node.getId());
                    adapter.getItem(1).addNode(node1);
                }

                database.getNodeChildDao().insert(nodeChild);

            }
        }
    }

    private void fetchData() {
        nodesParents = database.getNodeChildDao().getParentsForChild(node.getId());
        nodesChildren = database.getNodeChildDao().getChildrenForParent(node.getId());
    }

    private void sendArray(ViewPagerAdapter adapter, String title, ArrayList<Node> nodes) {
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("Node", nodes);
        bundle.putBoolean("Child", currentTab);
        bundle.putLong("Boss", node.getId());
        MyFragment fragmentGet = new MyFragment();
        fragmentGet.setArguments(bundle);
        adapter.addFragment(fragmentGet, title);
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());

        sendArray(adapter, "Parents", (ArrayList<Node>) nodesParents);
        sendArray(adapter, "Children", (ArrayList<Node>) nodesChildren);

        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<MyFragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public MyFragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(MyFragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

}
