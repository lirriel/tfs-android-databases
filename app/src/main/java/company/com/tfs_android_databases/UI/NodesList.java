package company.com.tfs_android_databases.UI;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import company.com.tfs_android_databases.Database.AppDatabase;
import company.com.tfs_android_databases.Database.utils.DatabaseInitializer;
import company.com.tfs_android_databases.Entity.Node;
import company.com.tfs_android_databases.R;

public class NodesList extends AppCompatActivity {

    private AppDatabase database;
    RecyclerView recyclerView;
    MyAdapter myAdapter;

    private static final String EXTRA = "ID";
    private long id;

    public static void startForResult(Activity activity, int requestCode, long id) {
        Intent intent = new Intent(activity, NodesList.class);
        intent.putExtra(EXTRA, id);
        activity.startActivityForResult(intent, requestCode);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nodes_list);
        id = getIntent().getLongExtra(EXTRA, 1);

        recyclerView = initialize();
        settings(recyclerView);

        database = AppDatabase.getAppDatabase(getApplicationContext());
        fetchData();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        database = AppDatabase.getAppDatabase(getApplicationContext());
        fetchData();
    }

    @Override
    protected void onDestroy() {
        AppDatabase.destroyInstance();
        super.onDestroy();
    }

    private void fetchData() {
        List<Node> nodes = database.getNodeDao().getAllNodes();
        myAdapter = new MyAdapter(node -> {
            long _id = node.getId();
            Intent intent = new Intent();
            intent.putExtra(EXTRA, _id);
            setResult(RESULT_OK, intent);
            finish();
        }, database, getResources());
        myAdapter.setData(nodes);

        recyclerView.setAdapter(myAdapter);
    }

    private RecyclerView initialize() {
        return  findViewById(R.id.recycler_view_2);
    }

    private void settings(RecyclerView recyclerView) {
        recyclerView.setHasFixedSize(false);

        setLayoutManager(recyclerView);
        setItemDecorator(recyclerView);
    }

    private void setItemDecorator(RecyclerView recyclerView) {
        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(itemDecoration);
    }

    private void setLayoutManager(RecyclerView recyclerView) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
    }
}
