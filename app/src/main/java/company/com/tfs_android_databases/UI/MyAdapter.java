package company.com.tfs_android_databases.UI;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import company.com.tfs_android_databases.Database.AppDatabase;
import company.com.tfs_android_databases.Entity.Node;
import company.com.tfs_android_databases.R;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    private List<Node> nodes;
    private OnItemClickListener listener;
    private static MyClickListener deleteListener;
    private AppDatabase appDatabase;
    private static int red, yellow, blue;
    static boolean add = true;

    AppDatabase getAppDatabase() {
        return appDatabase;
    }

    MyAdapter(OnItemClickListener listener, AppDatabase appDatabase, Resources r) {
        this.listener = listener;
        this.appDatabase = appDatabase;
        red = r.getColor(R.color.colorRed);
        blue = r.getColor(R.color.colorBlue);
        yellow = r.getColor(R.color.colorYellow);
    }

    void addNode(Node node) {
        nodes.add(node);
        notifyItemInserted(nodes.size() - 1);
    }

    public void setData(List<Node> newNodes) {
        nodes = newNodes;
        notifyDataSetChanged();
    }

    long getId(int index) {
        return nodes.get(index).getId();
    }

    void deleteItem(int index) {
        nodes.remove(index);
        notifyItemRemoved(index);
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.node, parent, false);
        return new ViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Node node = nodes.get(position);
        CheckConnections(node);
        holder.bind(node);
    }

    private void CheckConnections(Node node) {
        List<Node> l = appDatabase.getNodeChildDao().getChildrenForParent(node.getId());
        node.isParent = l.size() > 0;
        l = appDatabase.getNodeChildDao().getParentsForChild(node.getId());
        node.isChild = l.size() > 0;
    }

    @Override
    public int getItemCount() {
        if (nodes == null)
            return 0;

        return nodes.size();
    }

    void setOnItemClickListener(MyClickListener myClickListener) {
        deleteListener = myClickListener;
    }

    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        CardView cardView;
        TextView node_id;
        TextView node_value;

        OnItemClickListener listener;

        ViewHolder(View itemView, OnItemClickListener listener) {
            super(itemView);
            this.listener = listener;
            cardView = itemView.findViewById(R.id.card_view);
            node_id = itemView.findViewById(R.id.node_id);
            node_value = itemView.findViewById(R.id.node_value);
        }

        @SuppressLint("DefaultLocale")
        void bind(Node node) {
            if (listener != null && add)
                itemView.setOnClickListener(view -> listener.onClick(node));
            else if (listener != null && !add)
                itemView.setOnClickListener(this);
            int t = node.getValue();
            long t1 = node.getId();
            node_id.setText(String.format("Id: %d", t1));
            node_value.setText(String.format("Value: %d", t));
            setColor(node);
        }

        void setColor(Node node) {
            if (node.isChild && node.isParent)
                cardView.setBackgroundColor(red);
            else if (node.isChild)
                cardView.setBackgroundColor(yellow);
            else if (node.isParent)
                cardView.setBackgroundColor(blue);
            else
                cardView.setBackgroundColor(0);
        }

        @Override
        public void onClick(View v) {
            deleteListener.onItemClick(getAdapterPosition(), v);
        }
    }

    interface OnItemClickListener {
        void onClick(Node node);
    }

    interface MyClickListener {
        void onItemClick(int position, View v);
    }
}
