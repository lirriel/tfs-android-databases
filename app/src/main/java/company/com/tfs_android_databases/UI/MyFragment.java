package company.com.tfs_android_databases.UI;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import company.com.tfs_android_databases.Database.AppDatabase;
import company.com.tfs_android_databases.Entity.Node;
import company.com.tfs_android_databases.Entity.NodeChild;
import company.com.tfs_android_databases.R;

public class MyFragment extends Fragment implements MyAdapter.MyClickListener{
    private List<Node> nodes;
    private MyAdapter myAdapter;
    private boolean isChild = false;
    private long boss = 1;

    public MyFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void setLayoutManager(RecyclerView recyclerView) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(recyclerView.getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        if (bundle != null) {
            nodes = bundle.getParcelableArrayList("Node");
            isChild = bundle.getBoolean("Child");
            boss = bundle.getLong("Boss");
        }

        View view = inflater.inflate(R.layout.my_fragment, container, false);
        RecyclerView mRecyclerView = view.findViewById(R.id.recycler_view_1);
        setLayoutManager(mRecyclerView);
        myAdapter = new MyAdapter(null, AppDatabase.getAppDatabase(mRecyclerView.getContext()), getResources());
        MyAdapter.add = false;
        myAdapter.setOnItemClickListener(this);
        myAdapter.setData(nodes);
        mRecyclerView.setAdapter(myAdapter);
        return view;
    }

    @Override
    public void onItemClick(final int position, View v) {
        new AlertDialog.Builder(getActivity())
                .setTitle(getString(R.string.vertical_fragment_title_dialog_delete))
                .setMessage(getString(R.string.vertical_fragment_question_delete))
                .setPositiveButton(android.R.string.yes, (dialog, which) -> {
                    if (isChild)
                        myAdapter.getAppDatabase().getNodeChildDao().delete(new NodeChild(boss, myAdapter.getId(position)));
                    else
                        myAdapter.getAppDatabase().getNodeChildDao().delete(new NodeChild(myAdapter.getId(position), boss));

                    myAdapter.deleteItem(position);
                })
                .setNegativeButton(android.R.string.no, (dialog, which) -> {})
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void addNode(Node node) {
        myAdapter.addNode(node);
        myAdapter.notifyItemChanged(myAdapter.getItemCount());
    }
}