package company.com.tfs_android_databases.UI;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import java.util.List;

import company.com.tfs_android_databases.Database.AppDatabase;
import company.com.tfs_android_databases.Database.utils.DatabaseInitializer;
import company.com.tfs_android_databases.Entity.Node;
import company.com.tfs_android_databases.R;

public class MainActivity extends AppCompatActivity {

    private AppDatabase database;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // create database
        recyclerView = initialize();
        settings(recyclerView);

        database = AppDatabase.getAppDatabase(getApplicationContext());
        populateDb();
        fetchData();
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchData();
    }

    @Override
    protected void onDestroy() {
        AppDatabase.destroyInstance();
        super.onDestroy();
    }

    private void populateDb() {
        DatabaseInitializer.populateSync(database);
    }

    private void fetchData() {
        List<Node> nodes = database.getNodeDao().getAllNodes();
        MyAdapter myAdapter = new MyAdapter(node -> NodeInfo.start(MainActivity.this, node), database, getResources());
        myAdapter.setData(nodes);

        recyclerView.setAdapter(myAdapter);
        setListener(myAdapter, recyclerView);
    }

    private RecyclerView initialize() {
        return  findViewById(R.id.recycler_view);
    }

    private void settings(RecyclerView recyclerView) {
        recyclerView.setHasFixedSize(false);

        setLayoutManager(recyclerView);
        setItemDecorator(recyclerView);
    }

    private void setItemDecorator(RecyclerView recyclerView) {
        RecyclerView.ItemDecoration itemDecoration = new
                DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(itemDecoration);
    }

    private void setLayoutManager(RecyclerView recyclerView) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    private void setListener(final MyAdapter myAdapter, final RecyclerView recyclerView) {
        FloatingActionButton fab = findViewById(R.id.fab);
        final Context context = this;

        fab.setOnClickListener(view -> {
            LayoutInflater layoutInflaterAndroid = LayoutInflater.from(context);
            View mView = layoutInflaterAndroid.inflate(R.layout.input_dialog, null);
            AlertDialog.Builder alertDialogBuilderUserInput = new AlertDialog.Builder(context);
            alertDialogBuilderUserInput.setView(mView);

            final EditText inputDialog = mView.findViewById(R.id.userInputDialog);

            alertDialogBuilderUserInput
                    .setCancelable(false)
                    .setPositiveButton("Enter", (dialogBox, id) -> {
                        // ToDo get user input here
                        String s = String.valueOf(inputDialog.getText());

                        Node node = new Node(database.getNodeDao().countNodes() + 1, Integer.parseInt(s));

                        database.getNodeDao().insert(node);

                        myAdapter.addNode(node);
                        myAdapter.notifyDataSetChanged();
                        recyclerView.scrollToPosition(myAdapter.getItemCount() - 1);
                    })

                    .setNegativeButton("Cancel",
                            (dialogBox, id) -> dialogBox.cancel());

            AlertDialog alertDialogAndroid = alertDialogBuilderUserInput.create();
            alertDialogAndroid.show();
        });
    }

}
