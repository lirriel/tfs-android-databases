package company.com.tfs_android_databases.Dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import company.com.tfs_android_databases.Entity.Node;
import company.com.tfs_android_databases.Entity.NodeChild;

@Dao
public interface NodeChildDao {
    @Insert
    void insert(NodeChild nodeChild);

    @Query("SELECT COUNT(*) from nodechild where nodeId=:nodeID AND childId=:childID")
    int getConnection(long nodeID, long childID);

    @Query("DELETE FROM nodechild")
    void deleteAll();

    @Delete
    void delete(NodeChild nodeChild);

    @Query("SELECT COUNT(*) from nodechild")
    int count();

    @Query("SELECT * FROM node INNER JOIN nodechild ON node.id=nodechild.nodeId WHERE nodechild.childId=:childID")
    List<Node> getParentsForChild(final long childID);

    @Query("SELECT * FROM node INNER JOIN nodechild ON node.id=nodechild.childId WHERE nodechild.nodeId=:nodeID")
    List<Node> getChildrenForParent(final long nodeID);
}
