package company.com.tfs_android_databases.Dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import company.com.tfs_android_databases.Entity.Node;

@Dao
public interface NodeDao {
    @Insert
    void insertAll(Node... nodes);

    @Query("DELETE FROM node")
    void deleteAll();

    @Delete
    void delete(Node person);

    @Insert
    void insert(Node node);

    // Получение всех Person из бд
    @Query("SELECT * FROM node")
    List<Node> getAllNodes();

    @Query("SELECT COUNT(*) from node")
    int countNodes();

    @Query("SELECT * FROM node WHERE id=:id")
    Node getNode(long id);

    // Получение всех Person из бд с условием
    @Query("SELECT * FROM node WHERE value=:NodeValue")
    List<Node> getAllNodesWithValue(int NodeValue);
}
