package company.com.tfs_android_databases.Entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

@Entity(tableName = "node")
public class Node implements Parcelable{
    @PrimaryKey
    @ColumnInfo(name = "id")
    public long id;
    @ColumnInfo(name = "value")
    public int value;
    @Ignore
    public boolean isParent = false;
    @Ignore
    public boolean isChild = false;

    public Node(long id, int value) {
        this.id = id;
        this.value = value;
    }

    @Ignore
    protected Node(Parcel in) {
        id = in.readLong();
        value = in.readInt();
    }

    @Ignore
    public static final Creator<Node> CREATOR = new Creator<Node>() {
        @Override
        public Node createFromParcel(Parcel in) {
            return new Node(in);
        }

        @Override
        public Node[] newArray(int size) {
            return new Node[size];
        }
    };

    @Ignore
    public long getId() {
        return id;
    }
    @Ignore
    public void setId(long id) {
        this.id = id;
    }
    @Ignore
    public int getValue() {
        return value;
    }
    @Ignore
    public void setValue(int value) {
        this.value = value;
    }

    @Ignore
    @Override
    public int describeContents() {
        return 0;
    }

    @Ignore
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeInt(value);
    }
}

