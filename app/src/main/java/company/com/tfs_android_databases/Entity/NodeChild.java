package company.com.tfs_android_databases.Entity;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;

@Entity(primaryKeys = {"nodeId", "childId"})
public class NodeChild {
    @ColumnInfo(name = "nodeId")
    public final long nodeId;
    @ColumnInfo(name = "childId")
    public final long childId;

    public NodeChild(final long nodeId, final long childId) {
        this.nodeId = nodeId;
        this.childId = childId;
    }
}