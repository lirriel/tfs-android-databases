package company.com.tfs_android_databases.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import company.com.tfs_android_databases.Dao.NodeChildDao;
import company.com.tfs_android_databases.Dao.NodeDao;
import company.com.tfs_android_databases.Entity.Node;
import company.com.tfs_android_databases.Entity.NodeChild;

@Database(entities = {Node.class, NodeChild.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase INSTANCE;

    public abstract NodeDao getNodeDao();

    public abstract NodeChildDao getNodeChildDao();

    public static AppDatabase getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                    Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "Database")
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

}