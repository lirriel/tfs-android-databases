package company.com.tfs_android_databases.Database.utils;

import android.arch.persistence.room.RoomDatabase;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import java.util.Random;

import company.com.tfs_android_databases.Database.AppDatabase;
import company.com.tfs_android_databases.Entity.Node;
import company.com.tfs_android_databases.Entity.NodeChild;

public class DatabaseInitializer {

    // Simulate a blocking operation delaying each Loan insertion with a delay:
    private static final int DELAY_MILLIS = 500;

    public static void populateAsync(final AppDatabase db) {
        PopulateDbAsync task = new PopulateDbAsync(db);
        task.execute();
    }

    public static void populateSync(@NonNull final AppDatabase db) {
        populateWithTestData(db);
    }

    private static void addNode(final AppDatabase db, final Node node) {
        db.getNodeDao().insertAll(node);
    }

    private static void addConnection(final AppDatabase db, final NodeChild node) {
        db.getNodeChildDao().insert(node);
    }

    private static void populateWithTestData(AppDatabase db) {
        db.getNodeDao().deleteAll();
        db.getNodeChildDao().deleteAll();

        for (int i = 0; i < 10; i++) {
            Node node = new Node(i+1, i+1);
            addNode(db, node);
            try {
                Thread.sleep(DELAY_MILLIS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        NodeChild nodeChild = new NodeChild(1, 5);
        addConnection(db, nodeChild);
        nodeChild = new NodeChild(2, 5);
        addConnection(db, nodeChild);
        nodeChild = new NodeChild(1, 6);
        addConnection(db, nodeChild);
    }

    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final AppDatabase mDb;

        PopulateDbAsync(AppDatabase db) {
            mDb = db;
        }

        @Override
        protected Void doInBackground(final Void... params) {
            populateWithTestData(mDb);
            return null;
        }

    }
}
